$(document).ready(function () {
    if(localStorage.getItem('oxidGregPopupAccepted')){
        $(".popup-container").hide();
    }
})

$(".accept").click(function () {
    $(".popup-container").slideUp();
    localStorage.setItem('oxidGregPopupAccepted', 'shown');
});

$(".cancel").click(function () {
    window.alert('In order to exit this site please close this tab or whole browser.')
    localStorage.removeItem('oxidGregPopupAccepted', 'shown');
});