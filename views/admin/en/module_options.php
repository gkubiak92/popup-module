<?php

$sLangName = 'English';

$aLang = array(
    'charset' => 'UTF-8',
    'SHOP_MODULE_GROUP_popup_settings' => 'Popup settings',
    'SHOP_MODULE_sPopupTitle' => 'Title',
    'HELP_SHOP_MODULE_sPopupTitle' => 'Enter text you want to display on popup title',
    'SHOP_MODULE_sPopupDescription' => 'Description',
    'HELP_SHOP_MODULE_sPopupDescription' => 'Enter text you want to display below popup title',
    'SHOP_MODULE_sOkButtonText' => 'Text on Accept button',
    'HELP_SHOP_MODULE_sOkButtonText' => 'Enter text you want to display in accept button',
    'SHOP_MODULE_sCancelButtonText' => 'Text on Cancel button',
    'HELP_SHOP_MODULE_sCancelButtonText' => 'Enter text you want to display in cancel button'
);
