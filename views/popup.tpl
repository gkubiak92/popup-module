[{$smarty.block.parent}]

[{oxstyle include=$oViewConf->getModuleUrl('popup-module', 'out/css/popup.min.css')}]
[{oxscript include=$oViewConf->getModuleUrl('popup-module', 'out/js/popup-script.min.js')}]

[{assign var="oConfig" value=$oViewConf->getConfig()}]
[{assign var="sPopupTitle" value=$oConfig->getConfigParam('sPopupTitle')}]
[{assign var="sPopupDescription" value=$oConfig->getConfigParam('sPopupDescription')}]
[{assign var="sOkButtonText" value=$oConfig->getConfigParam('sOkButtonText')}]
[{assign var="sCancelButtonText" value=$oConfig->getConfigParam('sCancelButtonText')}]

<div class="popup-container">
    <div class="popup-window">
        <h1>[{$sPopupTitle}]</h1>
        <p>[{$sPopupDescription}]</p>
        <button class="btn btn-primary accept">[{$sOkButtonText}]</button>
        <button class="btn btn-danger cancel">[{$sCancelButtonText}]</button>
    </div>
</div>
