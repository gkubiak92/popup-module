<?php

/**
 * @package       popup
 * @category      module
 * @author        Grzegorz Kubiak
 * @licenses      GNU GENERAL PUBLIC LICENSE. More info can be found in LICENSE file.
 * @copyright (C) Grzegorz Kubiak 2019
 */

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id' => 'popup-module',
    'title' => 'Popup main page',
    'description' => array(
        'de' => 'Displays popup on main page',
        'en' => 'Displays popup on main page',
    ),
    'version' => '1.0.0',
    'author' => 'Grzegorz Kubiak',
    'email' => 'gkubiak92@gmail.com',
    'blocks' => array(
    array('template' => 'layout/header.tpl', 'block' => 'header_main', 'file' => '/views/popup.tpl'),
),
    'settings' => array(
    array('group' => 'popup_settings', 'name' => 'sPopupTitle', 'type' => 'str', 'value' => 'Example title'),
    array('group' => 'popup_settings', 'name' => 'sPopupDescription', 'type' => 'str', 'value' => 'Lorem ipsum dolor...'),
    array('group' => 'popup_settings', 'name' => 'sOkButtonText', 'type' => 'str', 'value' => 'Enter Site'),
    array('group' => 'popup_settings', 'name' => 'sCancelButtonText', 'type' => 'str', 'value' => 'Exit Site'),
)
);
